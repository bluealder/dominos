import {Component, OnInit} from '@angular/core';
import * as vis from 'vis';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Application} from './application.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'impact-analysis';
  applicationForm: FormGroup;
  serverForm: FormGroup;

  selectedView: string;
  selectedApplication: Application;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.applicationForm = this.formBuilder.group({
      platform: ['', Validators.required],
      application: ['', Validators.required]
    });

    this.serverForm = this.formBuilder.group({
      server: ['', Validators.required]
    });

  }

  changeView(view: string, server) {
    this.selectedView = view;
    this.serverForm.setValue({server: server});
    this.generateVisualisation();

  }

  generateVisualisation() {
    console.log(this.selectedView);
    if (this.selectedView === 'platform') {
      if (this.applicationForm.valid && this.applicationForm.value.application === 'cardControls') {
        const nodes = new vis.DataSet([
          {id: 1, label: 'Card Controls', color: '#ff8878'},
          {id: 2, label: 'SPS Portal'},
          {id: 3, label: 'EFT Portal'},
          {id: 4, label: 'Database'},
          {id: 5, label: 'Switch AUX'},
          {id: 6, label: 'Switch Pos'},
          {id: 7, label: 'Mobile Apps'},

        ]);

        const edges = new vis.DataSet([
          {from: 1, to: 4},
          {from: 2, to: 1},
          {from: 3, to: 1},
          {from: 5, to: 4},
          {from: 6, to: 4},
          {from: 7, to: 1},
        ]);

        const network = displayGraph(nodes, edges);
        network.on('click', (properties) => {
          console.log(properties);
          if (properties.nodes[0] === 1) {
            this.selectedApplication = {
              name: 'Card Controls',
              clients: ['CUA', 'TMB', 'FB'],
              server: 'PRODAPP20',
              productOwner: 'The coolest product owner'
            };
          } else if (properties.nodes[0] === 2) {
            this.selectedApplication = {
              name: 'SPS Portal',
              clients: ['ING', 'Bendigo', 'CUA'],
              server: 'EFTPOS20',
              productOwner: 'Joe from accounting'
            };
          } else {
            this.selectedApplication = null;
          }
        });
      } else if (this.applicationForm.valid && this.applicationForm.value.application === 'authentic') {
        const nodes = new vis.DataSet([
          {id: 1, label: 'Authentic', color: 'green'},
          {id: 2, label: 'Pin Status'},
          {id: 3, label: 'Message Broker'},
          {id: 4, label: 'VISA'},
          {id: 5, label: 'E-Hub'},
          {id: 6, label: 'MasterCard'},
          {id: 7, label: 'Bilat'},
          {id: 8, label: 'Fraud'},
          {id: 9, label: 'CBS'},
          {id: 10, label: 'Aux Database'}
        ]);

        const edges = new vis.DataSet([
          {from: 2, to: 1},
          {from: 3, to: 1},
          {from: 4, to: 1},
          {from: 5, to: 1},
          {from: 6, to: 1},
          {from: 7, to: 1},
          {from: 8, to: 1},
          {from: 9, to: 1},
          {from: 10, to: 1},
        ]);

        const network = displayGraph(nodes, edges);
      }
    } else if (this.selectedView === 'server') {
      if (this.serverForm.valid) {
        const nodes = new vis.DataSet([
          {id: 1, label: 'PRODAPP20', color: '#ff8878'},
          {id: 2, label: 'PAYS'},
          {id: 3, label: 'EFT Portal'},
          {id: 4, label: 'Transaction Service'},
          {id: 5, label: 'B2B Services'}
        ]);

        const edges = new vis.DataSet([
          {from: 2, to: 1},
          {from: 3, to: 1},
          {from: 4, to: 1},
          {from: 5, to: 1}
        ]);

        const network = displayGraph(nodes, edges);
      }
    }
  }
}

function displayGraph(nodes, edges) {
  const container = document.getElementById('mynetwork');
  const data = {
    nodes: nodes,
    edges: edges
  };
  const options = {
    edges: {
      arrows: 'to'
    }
  };
  return new vis.Network(container, data, options);
}
